package com.moumene.bankrestgraphql.services;

import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.dtos.AccountResponseDTO;

import java.util.List;

public interface AccountService {

    List<AccountResponseDTO> listAccounts();
    AccountResponseDTO findById(String id);
    AccountResponseDTO addAccount(AccountRequestDTO accountRequestDTO);
    AccountResponseDTO updateAccount(String id, AccountRequestDTO accountRequestDTO);
    AccountResponseDTO deleteAccount(String id);
}
