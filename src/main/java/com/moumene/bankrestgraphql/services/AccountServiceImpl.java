package com.moumene.bankrestgraphql.services;

import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.dtos.AccountResponseDTO;
import com.moumene.bankrestgraphql.entities.Account;
import com.moumene.bankrestgraphql.exceptions.AccountNotFoundException;
import com.moumene.bankrestgraphql.mappers.AccountMapper;
import com.moumene.bankrestgraphql.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<AccountResponseDTO> listAccounts() {
        List list = accountRepository.findAll().stream().map(accountMapper::accountToAccountResponseDTO).collect(Collectors.toList());
        return list;
    }

    @Override
    public AccountResponseDTO findById(String id) {
        return accountMapper.accountToAccountResponseDTO(accountRepository.findById(id).get());

    }

    @Override
    public AccountResponseDTO addAccount(AccountRequestDTO accountRequestDTO) {
        Account accountToSave = accountMapper.accountRequestDTOToAccount(accountRequestDTO);
        accountToSave.setId(UUID.randomUUID().toString());
        accountToSave.setCreatedAt(new Date());
        AccountResponseDTO accountResponseDTO = accountMapper.accountToAccountResponseDTO(accountRepository.save(accountToSave));
        return accountResponseDTO;
    }

    @Override
    public AccountResponseDTO updateAccount(String id, AccountRequestDTO accountRequestDTO) {
        Account accountToSave = accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));
        accountToSave.setAccountType(accountRequestDTO.getAccountType());
        accountToSave.setBalance(accountRequestDTO.getBalance());
        accountToSave.setCurrency(accountRequestDTO.getCurrency());
        accountMapper.accountRequestDTOToAccount(accountRequestDTO);
        AccountResponseDTO accountResponseDTO = accountMapper.accountToAccountResponseDTO(accountRepository.save(accountToSave));
        return accountResponseDTO;
    }

    @Override
    public AccountResponseDTO deleteAccount(String id) {
        AccountResponseDTO accountResponseDTO = accountMapper.accountToAccountResponseDTO(accountRepository.findById(id).get());
        accountRepository.deleteById(id);
        return accountResponseDTO;
    }

}
