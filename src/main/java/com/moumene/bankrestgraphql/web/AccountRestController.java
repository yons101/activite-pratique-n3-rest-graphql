package com.moumene.bankrestgraphql.web;

import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.dtos.AccountResponseDTO;
import com.moumene.bankrestgraphql.services.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class AccountRestController {

    private AccountService accountService;


    @GetMapping("/accounts")
    public List<AccountResponseDTO> accounts() {
        return accountService.listAccounts();
    }

    @GetMapping("accounts/{id}")
    public AccountResponseDTO account(@PathVariable String id) {
        return accountService.findById(id);
    }

    @PostMapping("accounts")
    public AccountResponseDTO add(AccountRequestDTO accountRequestDTO) {
        return accountService.addAccount(accountRequestDTO);
    }

    @PutMapping("accounts/{id}")
    public AccountResponseDTO update(@PathVariable String id, @RequestBody AccountRequestDTO account) {
        return accountService.updateAccount(id, account);
    }

    @DeleteMapping("accounts/{id}")
    public void delete(@PathVariable String id) {
        accountService.deleteAccount(id);
    }


}
