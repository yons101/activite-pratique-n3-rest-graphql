package com.moumene.bankrestgraphql.web;

import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.dtos.AccountResponseDTO;
import com.moumene.bankrestgraphql.entities.Account;
import com.moumene.bankrestgraphql.exceptions.AccountNotFoundException;
import com.moumene.bankrestgraphql.repositories.AccountRepository;
import com.moumene.bankrestgraphql.services.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@AllArgsConstructor
public class GraphQLController {

    private AccountRepository accountRepository;

    private AccountService accountService;

    @QueryMapping
    List<Account> accountList() {
        return accountRepository.findAll();
    }

    @QueryMapping
    Account accountById(@Argument String id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));
    }

    @MutationMapping
    public AccountResponseDTO addAccount(@Argument AccountRequestDTO account) {
        return accountService.addAccount(account);
    }

    @MutationMapping
    public AccountResponseDTO updateAccount(@Argument String id, @Argument AccountRequestDTO account) {
        return accountService.updateAccount(id, account);
    }

    @MutationMapping
    public AccountResponseDTO deleteAccount(@Argument String id) {
        return accountService.deleteAccount(id);
    }

}
