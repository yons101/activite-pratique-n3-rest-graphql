package com.moumene.bankrestgraphql.dtos;

import com.moumene.bankrestgraphql.enums.AccountType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountRequestDTO {
    private double balance;
    private String currency;
    private AccountType accountType;
}
