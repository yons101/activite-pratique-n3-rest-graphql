package com.moumene.bankrestgraphql.enums;

public enum AccountType {
    SAVING_ACCOUNT, CURRENT_ACCOUNT

}
