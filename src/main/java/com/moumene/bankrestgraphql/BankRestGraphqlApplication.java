package com.moumene.bankrestgraphql;

import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.enums.AccountType;
import com.moumene.bankrestgraphql.services.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class BankRestGraphqlApplication {

    private AccountService accountService;

    public BankRestGraphqlApplication(AccountService accountService) {
        this.accountService = accountService;
    }

    public static void main(String[] args) {
        SpringApplication.run(BankRestGraphqlApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner() {
        return args -> {
            accountService.addAccount(new AccountRequestDTO(1000, "EUR", AccountType.SAVING_ACCOUNT));
            accountService.addAccount(new AccountRequestDTO(1000, "MAD", AccountType.CURRENT_ACCOUNT));
            accountService.addAccount(new AccountRequestDTO(1000, "USD", AccountType.CURRENT_ACCOUNT));
        };
    }

}
