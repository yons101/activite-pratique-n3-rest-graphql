package com.moumene.bankrestgraphql.repositories;

import com.moumene.bankrestgraphql.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {
}
