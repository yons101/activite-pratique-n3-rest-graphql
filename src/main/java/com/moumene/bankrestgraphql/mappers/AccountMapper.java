package com.moumene.bankrestgraphql.mappers;


import com.moumene.bankrestgraphql.dtos.AccountRequestDTO;
import com.moumene.bankrestgraphql.dtos.AccountResponseDTO;
import com.moumene.bankrestgraphql.entities.Account;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {
    public AccountResponseDTO accountToAccountResponseDTO(Account account) {
        AccountResponseDTO accountResponseDTO = new AccountResponseDTO();
        BeanUtils.copyProperties(account, accountResponseDTO);
        return accountResponseDTO;
    }

    public Account accountRequestDTOToAccount(AccountRequestDTO accountRequestDTO) {
        Account account = new Account();
        BeanUtils.copyProperties(accountRequestDTO, account);
        return account;
    }
}
